//������ �����
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#define N 100

void clean_stdin()
{
	int c;
	do {
		c = getchar();
	} while (c != '\n' && c != EOF);
}

int main()
{
	int guess, secret;
	srand(time(NULL));
	secret = rand() % N + 1;
	do
	{
		while (1)
		{
			printf("Guess the number! (from 1 to %d):\n", N);
			scanf("%d", &guess);
			if (guess < 1 || guess > N)
			{
				puts("Input error!\n");
				clean_stdin();
			}
			else break;
		}
		if (secret < guess) puts("It's less!\n");
		else
			if (secret > guess) puts("It's more!\n");
	} while (secret != guess);
	puts("Congratulations!\n");
	return 0;
}