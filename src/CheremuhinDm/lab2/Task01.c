// ������� �����
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <windows.h>

void clean_stdin()
{
	int c;
	do {
		c = getchar();
	} while (c != '\n' && c != EOF);
}

int main()
{
	clock_t start, now;
	int t = 0, H = 0;
	float h = .0f, g = 9.81f, D;
	while (1)
	{
		puts("Enter distance in meters.(<10000)");
		scanf("%d", &H);
		if (H > 0 && H <= 10000)
			break;
		else
		{
			puts("Input error!");
			clean_stdin();
		}
	}
	start = clock();
	while (1)
	{
		now = clock();
		t = (now - start) / 1000;
		Sleep(1000);
		D = (H - g * t * t / 2);
		if (D > 0)
		{
			printf("t = %.2d c, h = %.1f m\n", t, D);
			while (clock() < now + 1000);
		}
		else
		{
			puts("BABAH!");
			break;
		}
	}
}