// ����� ����� �� ������
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>

void clean_stdin()
{
	int c;
	do {
		c = getchar();
	} while (c != '\n' && c != EOF);
}

int main()
{
	int i = 0, num = 0, count = 0;
	char text[1000];
	// ���� ������
	puts("Type your text: ");
	fgets(text, 999, stdin);
	puts("\n");	
	while (1)
	{
		puts("Enter the number of word.");
		scanf("%d", &num);
		if (num >= 0 && num <= 100)
			break;
		else
		{
			puts("Input error!");
			clean_stdin();
		}
	}
	// ������� ���� � �����
	if (text[i] == '\n')
	{
		puts("No words!");
		return 0;
	}
	i = 0;
	do {
		if (text[i] != ' ')
		{
			count++;
			if (count == num)
			{
				do {
					putchar(text[i]);
					i++;
				} while (text[i] != ' ' && i < strlen(text));
			}
			else {
				do {
					i++;
				} while (text[i] != ' ' && i < strlen(text));
			}
		}
		else
			i++;
	} while (i < strlen(text));
	putchar('\n\n');
	if (count < num)
		printf("No word number %d!\n", num);
	return 0;
}