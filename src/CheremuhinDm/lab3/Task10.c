// �������� ����� �� ������
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>

void clean_stdin()
{
	int c;
	do {
		c = getchar();
	} while (c != '\n' && c != EOF);
}

int main()
{
	int i = 0, num = 0, count = 0, index = 0;
	char text[1000];
	// ���� ������
	puts("Type your text: ");
	fgets(text, 999, stdin);
	puts("\n");
	while (1)
	{
		puts("Enter the number of word.");
		scanf("%d", &num);
		if (num >= 0 && num <= 100)
			break;
		else
		{
			puts("Input error!");
			clean_stdin();
		}
	}
	// �������� ����� � �����
	if (text[i] == '\n')
	{
		puts("No words");
		return 0;
	}
	i = 0;
	do {
		if (text[i] != ' ')
		{
			count++;
			if (count == num)
			{
				do {
					index++;
					i++;
				} while (text[i] != ' ' && i < strlen(text));
				do {
					text[i-index] = text[i];
					i++;
				} while (i < strlen(text));
				text[i-index] = 0;
			}
			else {
				do {
					i++;
				} while (text[i] != ' ' && i < strlen(text));
			}
		}
		else
			i++;
	} while (i < strlen(text));
	putchar('\n\n');
	// ����� ������
	if (count >= num)
		printf("%s\n", text);
	else
		printf("No word number %d!\n", num);
	return 0;
}