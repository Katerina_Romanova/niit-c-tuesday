/* ��������� ������������ � ������������ �����
� ������ ����� � ���� ����������� "������ ����!" "������ ����!"
� �.�. ������� ����������� �������� ��������� ���������.*/
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>

void clean_stdin()
{
	int c;
	do
	{
		c = getchar();
	} while (c != '\n' && c != EOF);	
}

int main()
{
	int hours = 0, minutes = 0, seconds = 0, t_summ_1 = 0, t_summ_2 = 0, t_summ_3 = 0, t_summ_4 = 0, t_ext_i = 0;
	int edge_1_H = 04, edge_1_M = 00, edge_1_S = 00, 
		edge_2_H = 10, edge_2_M = 00, edge_2_S = 00,
		edge_3_H = 16, edge_3_M = 00, edge_3_S = 00,
		edge_4_H = 22, edge_4_M = 00, edge_4_S = 00;
			
	while (1)
		{
			puts("Hello, master! What time is it now?\nIn format HH:MM:SS, please!");
			scanf("%d:%d:%d", &hours, &minutes, &seconds);
			if (hours >= 0 && hours <= 23 && minutes >= 0 && minutes <= 59 && seconds >= 0 && seconds <= 59)
			{
				t_summ_1 = ((edge_1_H * 60 * 60) + (edge_1_M * 60) + edge_1_S);
				t_summ_2 = ((edge_2_H * 60 * 60) + (edge_2_M * 60) + edge_2_S);
				t_summ_3 = ((edge_3_H * 60 * 60) + (edge_3_M * 60) + edge_3_S);
				t_summ_4 = ((edge_4_H * 60 * 60) + (edge_4_M * 60) + edge_4_S);
				t_ext_i = (hours * 60 * 60 + minutes * 60 + seconds);			
					if (t_ext_i >= t_summ_4 || t_ext_i < t_summ_1)
						puts("Good night, master!");
					else
					{
						if (t_ext_i >= t_summ_1 && t_ext_i < t_summ_2)
							puts("Good morning, master!");
						else
						{
							if (t_ext_i >= t_summ_2 && t_ext_i < t_summ_3)
								puts("Good day, master!");
							else
								puts("Good evening, master!");
						}
					}
					break;
			}
			else
				puts("Input error!");
			clean_stdin();
		}		
		return 0;
}