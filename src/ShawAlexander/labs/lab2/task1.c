/*
1. Написать программу, имитирующую работу высотомера бомбы. Бом-
ба падает с высоты H, которая задается пользователем. В любой
момент времени можно узнать пройденное расстояние по формуле
...
Высотомер бомбы срабатывает раз в секунду и выводит на терми-
нал текущее значение высоты над поверхностью земли h.
*/
#include <windows.h>
#include <stdio.h>
void cleanIn() {
    char c;
    do {
        c = getchar();
    } while (c != '\n' && c != EOF);
}
int main() {
    double h, g = 9.81;
    int t = 0;
    while (1) {
        puts("Enter a value of height:");
        if (scanf("%lf", &h) == 1)
            break;
        else {
            puts("Input error!");
            cleanIn();
        }
    }
    while (1) {
        double distance = (g * (t * t)) / 2;
        if (distance > h) {
            puts("BOOM!");
            break;
        }
        printf("t = %02ds H = %06.1lf\n", t, h - distance);
        Sleep(1000);
        t++;
    }
    return 0;
}