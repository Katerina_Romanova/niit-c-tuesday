/*
6. �������� ���������, ��������� ������ �� ������ ��������. ������� ���-
������ ������� � ������ ������, � ����� ������ � ������� ����� �������,
���� �� ���������� ������ 1.
*/
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#define SIZE 100

int main() {
    char string[SIZE];
    fgets(string, SIZE, stdin);
    int start = 0;
    int end = strlen(string) - 1;
    while (isspace(string[start])) {
        start++;
    }
    while (isspace(string[end])) {
        end--;
    }
    for (int i = 0; i < sizeof(string); i++) { //�������� �������� �� ������ � ����� ������
        if (start != end + 1) {
            string[i] = string[start];
            start++;

        }
        else {
            if (string[i] == 0)
                break;
            else
                string[i] = 0;
        }
    }

    int spacePut = 0;

    for (int i = 0; i < strlen(string);)
    {
        char c = string[i];
        if (isspace(c)) {
            if (spacePut == 0) { //���� ��� - ������ ������
                spacePut = 1;
                i++;
            }
            else {
                int start, end = strlen(string) - 1;
                for (int q = i + 1; q < strlen(string); q++) { //������� start - ������ ������ ����� ���� ��������
                    if (!isspace(string[q])) {
                        start = q;
                        break;
                    }
                }
                for (int k = i; i < end; k++) { //���������� �������� � ��������� string[start]-string[end] �� ��������, ������������ � ������� �������;
                    if (start <= end) {         //����� ����������� ������������� ��� �������� � ������ ������� '\0'
                        string[k] = string[start];
                        start++;
                    }
                    else {
                        if (string[k] != 0)
                            string[k] = 0;
                        else
                            break;
                    }
                }
                spacePut = 0;
            }
        }
        else {
            i++;
            spacePut = 0;
        }
    }
    //��������
    printf("%s\n", string);
    return 0;
}