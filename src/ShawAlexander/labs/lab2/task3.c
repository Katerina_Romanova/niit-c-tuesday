/*
3. Написать программу, выводящую на экран треугольник из звёздочек
*/
#include <stdio.h>

int main() {
	int lines, baseSize, stars = 1;
	puts("Set the number of lines: ");
	scanf("%d", &lines);
	baseSize = lines + 2;
	for (int i = 0; i < lines; i++)
	{
		for (int s = i; s < lines - 1; s++) {
			putchar(' ');
		}
		baseSize -= 2;
		for (int i = 0; i < stars; i++)
			putchar('*');
		stars += 2;
		putchar('\n');
	}
	return 0;
}