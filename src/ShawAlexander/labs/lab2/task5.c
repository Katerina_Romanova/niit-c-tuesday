/*
5. Написать программу, которая выводит на экран 10 паролей, сгенерированных
случайным образом из латинских букв и цифр, причём буквы должны быть
как в нижнем, так и в верхнем регистрах. Длина пароля - 8 символов.
*/
#include <stdio.h>
#define SIZE 8 //Длина пароля
#define PASSWORDS 10

int main() {
	srand(time(0));
	char password[SIZE];
	int coin;
	char c;
	for (int i = 0, passes = 0; passes < PASSWORDS; i++)
	{
		coin = (rand() % 3);
		if (coin == 0)
			c = (rand() % 10) + 48;
		else if (coin == 1)
			c = (rand() % 26) + 97;
		else
			c = (rand() % 26) + 65;
		password[i] = c;

		if (i == SIZE-1) {
			for (int i = 0; i < SIZE; i++)
				putchar(password[i]);
			putchar('\n');
			i = -1;
			passes++;
		}
	}
	return 0;
}