/*
4. �������� ���������, ������� ������� ����� ����� �� �������� ������
���������:
��������� ������������� ����������� ������������������ ���� � ������ ���
����� � ������������ �� ��� ������ �����. � ��������� ������������� ������-
����� �� ������������ ����� ��������, �� ���� ���� ������������ ������ �����
������� ������������������ ����, � ����� ������� �� ��������� �����.
*/
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#define SIZE 100
#define MAX_N 6

int main()
{
    char string[SIZE] = { 0 };
    char number[SIZE] = { 0 };
    char startFound = 0;
    unsigned int sum = 0;
    puts("Enter a string:");
    fgets(string, SIZE, stdin);
    //������ �� ������
    for (int i = 0, start = 0; i < strlen(string); i++) {
        char c = string[i];
        if (isdigit(c) && !startFound) {
            startFound = 1;
            start = i;
        }
        else if ((!isdigit(c) && startFound) || (i+1) == SIZE) {
            startFound = 0;
            //���� � ����� ������ ���� ��������
            if (i - start <= MAX_N) {
                int n = 0;
                for (int q = start; q < i; q++, n++)
                {
                    number[n] = string[q];
                }
                number[n] = 0;
                int num = atoi(number);
                sum += num;
            }
            //���� � ����� ������ ����� ��������
            else {
                int nI = 0;
                int fullNumber = 0;
                //��������� ������ � ������
                for (int q = start; q < i; q++, nI++)
                {
                    number[nI] = string[q];
                }
                //������ �� ����� � ����� � 6 ����
                for (int n = 0; n < strlen(number); n += 6)
                {
                    //������ �� ����� ������; ���� ���� �������� ������, ��� �����, �� ������ �� ��������� �����
                    for (int d = n; n+6>strlen(number) ? d < strlen(number) : d < (n + 6); d++)
                    {
                        int digit;
                        //�������� ������ � ��������� �� ������� � ������
                        if (d != n) {
                            digit = number[d] - '0';
                            fullNumber = (fullNumber * 10) + digit;
                        }
                        //������������� ������ ������
                        else {
                            digit = number[n] - '0';
                            fullNumber = digit;
                        }
                    }
                    //���������� ����� � �����
                    sum += fullNumber;
                }
            }
        }
    }
    printf("%d\n", sum);
    return 0;
}